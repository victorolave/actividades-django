from rest_framework import serializers
from autenticacion.models import User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=68, min_length=6, write_only=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'password']

    def create(self, validated_data):
        email = validated_data['email']
        password = validated_data['password']

        return User.objects.create(email, password)