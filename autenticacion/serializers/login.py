from rest_framework import serializers
from django.contrib import auth
from rest_framework.exceptions import AuthenticationFailed

from autenticacion.models import User


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255, min_length=3)
    password = serializers.CharField(max_length=68, min_length=6, write_only=True)
    tokens = serializers.SerializerMethodField()

    def get_tokens(self, obj):
        user = User.objects.get(email=obj['email'])

        return {
            'refresh': user.tokens()['refresh'],
            'access': user.tokens()['access']
        }

    class Meta:
        model = User
        fields = ['email', 'password', 'tokens']

    def validate(self, attrs):

        email = attrs['email']
        password = attrs['password']

        user = auth.authenticate(email=email, password=password)

        if not user:
            raise AuthenticationFailed('Credenciales Invalidas')

        return {
            'email': user.email,
            'tokens': user.tokens()
        }

        return super().validate(attrs)