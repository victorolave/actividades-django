from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager, PermissionsMixin)
from django.db import models

from rest_framework_simplejwt.tokens import RefreshToken


class UserManager (BaseUserManager):
    def create(self, email, password):

        if email is None:
            raise TypeError('Los usuarios deben tener un email.')

        user = self.model(
            email=self.normalize_email(email)
        )

        user.set_password(password)
        user.save()

        return user


class User (AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def __str__(self):
        return self.email

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }

    class Meta:
        db_table = 'usuarios'