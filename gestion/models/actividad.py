from django.db import models

from autenticacion.models import User


class Actividad(models.Model):
    descripcion = models.CharField(max_length=255)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    creado = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'actividades'