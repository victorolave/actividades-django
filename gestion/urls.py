from django.urls import path
from . import views

urlpatterns = [
    path('', views.MyActivities.as_view(), name='restaurants'),
]
