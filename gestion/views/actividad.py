from rest_framework import permissions
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.utils import json

from gestion.models.actividad import Actividad
from gestion.serializers import ActividadSerializer

from gestion.permissions import EsPropietario


class MyActivities(ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated, EsPropietario)

    serializer_class = ActividadSerializer
    queryset = Actividad.objects

    def perform_create(self, serializer):
        return serializer.save(usuario=self.request.user)

    def get_queryset(self):

        data = self.queryset

        body_unicode = self.request.body.decode('utf-8')
        body = json.loads(body_unicode)

        if 'dia' in body:
            dia = body['dia']
            data = data.filter(creado__day=dia)

        if 'mes' in body:
            mes = body['mes']
            data = data.filter(creado__month=mes)

        if 'dia' not in body and 'mes' not in body:
            data = data.all()

        return data
