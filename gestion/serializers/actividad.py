from rest_framework import serializers

from autenticacion.serializers import UserSerializer
from gestion.models import Actividad


class ActividadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Actividad
        fields = ['descripcion', 'usuario', 'creado']
        extra_kwargs = {'usuario': {'required': False}}

    def to_representation(self, instance):
        self.fields['usuario'] = UserSerializer(read_only=True)
        return super(ActividadSerializer, self).to_representation(instance)
