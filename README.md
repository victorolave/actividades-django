## ACTIVIDADES - DJANGO

* * *

Esta es un API para la creación de actividades de un usuario. En esta el usuario puede registrarse y registrar sus actividades. También puede filtrarlas por el día y/o el mes que desee.

| Endpoint | Metodo | Body |
| --- | --- | --- |
| /autenticacion/registro | **POST** | ```json<br>{<br>    "email": "victorolave1131@gmail.com",<br>    "password": "victor1131"<br>}<br>``` |
| /autenticacion/login | **POST** | ```json<br>{<br>    "email": "victorolave1131@gmail.com",<br>    "password": "victor1131"<br>}<br>``` |
| /actividades | **POST** | ```json<br>{<br>    "descripcion": "Nueva Actividad"<br>}<br>``` |
| /actividades | **GET** | ```json<br>{<br>    "dia": "21",<br>    "mes": "4"<br>}<br>``` |
